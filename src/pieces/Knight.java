/**
 * @author Jerry Zhu and Chris Hartley
 */
package pieces;

import chess.Chessboard;

public class Knight extends Piece{
	
	
	/**
	 * Creates a knight of the chosen color
	 * @param color Player that the knight belongs to
	 */
	public Knight(String color){
		super(color);
	}
	
	
	/**
	 * Decides if the proposed move is valid for this knight
	 * @param oldPos Starting position
	 * @param newPos Proposed new position
	 * @param board Chessboard where the move would take place
	 * @return True if move is valid, false otherwise
	 */
	public boolean validMove(String oldPos, String newPos, Chessboard board) {
		
		//knight movement
		//System.out.println("Checking knight");
		
		int[] oldFileRank = fileRankToCoordinates(oldPos);
		int[] newFileRank = fileRankToCoordinates(newPos);
		//System.out.println(Math.abs(oldFileRank[0] - newFileRank[0]));
		//System.out.println(Math.abs(oldFileRank[1] - newFileRank[1]));
		
		if(Math.abs(oldFileRank[0] - newFileRank[0]) == 2 && Math.abs(oldFileRank[1] - newFileRank[1]) == 1 && !sameColorCheck(board.getTile(newPos)))
			return true;
		if(Math.abs(oldFileRank[1] - newFileRank[1]) == 2 && Math.abs(oldFileRank[0] - newFileRank[0]) == 1 && !sameColorCheck(board.getTile(newPos)))
			return true;
		return false;
	}

	public String toString() {
		return this.color.charAt(0)+"N";
	}
}