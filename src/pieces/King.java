/**
 * @author Jerry Zhu and Chris Hartley
 */
package pieces;

//import chess.Chess;
import chess.Chessboard;

public class King extends Piece {
	
	
	/**
	 * Creates a king of the chosen color
	 * @param color Player that the king belongs to
	 */
	public King(String color) {
		super(color);
	}
	
	
	/**
	 * Decides if the proposed move is valid for this king
	 * @param oldPos Starting position
	 * @param newPos Proposed new position
	 * @param board Chessboard where the move would take place
	 * @return True if move is valid, false otherwise
	 */
	public boolean validMove(String oldPos, String newPos, Chessboard board) {

		// need to check all the tiles around the King
		//System.out.println("Checking King");
		
		int[] oldFileRank = fileRankToCoordinates(oldPos);
		int[] newFileRank = fileRankToCoordinates(newPos);
		
		//if the difference between cols and rows is 0 or 1
		if (Math.abs(oldFileRank[0] - newFileRank[0]) <= 1 && Math.abs(oldFileRank[1] - newFileRank[1]) <= 1){
			
			//return if the target square is occupied by a friendly piece
			return !sameColorCheck(board.getTile(newPos));
		}
		
		//castle move check
		
		
		
		//condition: if the move is along the bottom or top of the board
		if(((oldFileRank[1] == 7 && newFileRank[1] == 7) && (newFileRank[0] == 6 || newFileRank[0] == 2)) ||
				((oldFileRank[1] == 0 && newFileRank[1] == 0)) && (newFileRank[0] == 6 || newFileRank[0] == 2)) {
			//System.out.println("castle move");
			int tempFileRank[] = new int[2];
			//if trying to castle on the kings side
			if(newFileRank[0] == 6) {
				tempFileRank[0] = newFileRank[0]+1;
				tempFileRank[1] = newFileRank[1];
			}
			//if trying to castle on the queens side
			if(newFileRank[0] == 2) {
				tempFileRank[0] = newFileRank[0]-2;
				tempFileRank[1] = newFileRank[1];
			}
			//if the King and Rook have not moved yet
			if(board.getTile(board.coordinatesToFileRank(tempFileRank)).getPiece() instanceof Rook &&
					board.getTile(board.coordinatesToFileRank(tempFileRank)).getPiece().moved == false && 
					board.getTile(oldPos).getPiece().moved == false) {
				return checkPiecesInBetween(oldFileRank,tempFileRank,"lr",board);
			}		
		}
		
		return false;
	}


	public String toString() {
		return this.color.charAt(0) + "K";
	}
}