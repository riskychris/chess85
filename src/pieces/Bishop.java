/**
 * @author Jerry Zhu and Chris Hartley
 */
package pieces;

import chess.Chessboard;

public class Bishop extends Piece {
	
	/**
	 * Creates a bishop of the chosen color
	 * @param color Player that the bishop belongs to
	 */
	public Bishop(String color) {
		super(color);
	}
	
	
	/**
	 * Decides if the proposed move is valid for this bishop
	 * @param oldPos Starting position
	 * @param newPos Proposed new position
	 * @param board Chessboard where the move would take place
	 * @return True if move is valid, false otherwise
	 */
	public boolean validMove(String oldPos, String newPos, Chessboard board) {
		// check for only diagonal movement
		// the idea is that both the horizontal and vertical movement will be the same
		// length for each valid move
		//System.out.println("Checking Bishop");

		int[] oldFileRank = fileRankToCoordinates(oldPos);
		int[] newFileRank = fileRankToCoordinates(newPos);

		if (Math.abs(oldFileRank[0] - newFileRank[0]) == Math.abs(oldFileRank[1] - newFileRank[1])
				&& !sameColorCheck(board.getTile(newPos))
				&& checkPiecesInBetween(oldFileRank, newFileRank, "dia", board)) {
			return true;
		}

		return false;
	}

	public String toString() {
		return this.color.charAt(0) + "B";
	}
}
