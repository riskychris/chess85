/**
 * @author Jerry Zhu and Chris Hartley
 */
package pieces;

import chess.Chessboard;

public class Queen extends Piece {
	
	
	/**
	 * Creates a queen of the chosen color
	 * @param color Player that the queen belongs to
	 */
	public Queen(String color) {
		super(color);
	}
	
	/**
	 * Decides if the proposed move is valid for this queen
	 * @param oldPos Starting position
	 * @param newPos Proposed new position
	 * @param board Chessboard where the move would take place
	 * @return True if move is valid, false otherwise
	 */
	public boolean validMove(String oldPos, String newPos, Chessboard board) {

		//System.out.println("Checking Queen");
		int[] oldFileRank = fileRankToCoordinates(oldPos);
		int[] newFileRank = fileRankToCoordinates(newPos);

		// if the queen is going vertical
		if (oldFileRank[0] == newFileRank[0] && !sameColorCheck(board.getTile(newPos))
				&& checkPiecesInBetween(oldFileRank, newFileRank, "ud", board)) {
			return true;
		}
		// if the queen is going horizontal
		if (oldFileRank[1] == newFileRank[1] && !sameColorCheck(board.getTile(newPos))
				&& checkPiecesInBetween(oldFileRank, newFileRank, "lr", board)) {
			return true;
		}
		//if the queen is going diagonal
		if (Math.abs(oldFileRank[0] - newFileRank[0]) == Math.abs(oldFileRank[1] - newFileRank[1])
				&& !sameColorCheck(board.getTile(newPos))
				&& checkPiecesInBetween(oldFileRank, newFileRank, "dia", board)) {
			return true;
		}

			return false;
	}

	public String toString() {
		return this.color.charAt(0) + "Q";
	}
}
