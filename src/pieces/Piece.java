/**
 * @author Jerry Zhu and Chris Hartley
 */
package pieces;

import chess.Chess;
import chess.Chessboard;
import chess.Tile;

public class Piece {
	
	/*
	 * Color of this piece
	 */
	public String color = "";
	
	/*
	 * Whether or not this piece has moved during this game
	 */
	public boolean moved = false;

	/**
	 * Creates a piece of the chosen color
	 * @param color Player that the piece belongs to
	 */
	public Piece(String color) {
		this.color = color.toLowerCase();
	}
	
	
	/**
	 * Retrieve color of this piece
	 * @return Color of this piece
	 */
	public String getColor() {
		return this.color;
	}

	
	/**
	 * Decides if the proposed move is valid for this piece
	 * @param oldPos Starting position
	 * @param newPos Proposed new position
	 * @param board Chessboard where the move would take place
	 * @return True if move is valid, false otherwise
	 */
	public boolean validMove(String oldPos, String newPos, Chessboard board) {
		// return coordinates of moves in an array = [col, row]
		// int[] oldCoordinates = fileRankToCoordinates(oldPos);
		// int[] newCoordinates = fileRankToCoordinates(newPos);

		// check if the change in coordinates is legal
		// then check if another piece is blocking that move

		// temporary to prevent error
		return true;
	}
	
	
	/**
	 * Decides if the proposed move is a valid capture for this piece
	 * @param oldPos Starting position
	 * @param newPos Proposed new position
	 * @param board Chessboard where the capture would take place
	 * @return True if capture is valid, false otherwise
	 */
	public boolean validCapture(String oldPos, String newPos, Chessboard board) {
		return validMove(oldPos, newPos, board);
	}
	
	
	/**
	 * Converts FileRank coordinates to integer coordinates
	 * @param fileRank The coordinates expressed in FileRank format
	 * @return A 2-element integer array corresponding to a Chessboard tile
	 */
	public int[] fileRankToCoordinates(String fileRank) {
		char file = fileRank.charAt(0);
		char rank = fileRank.charAt(1);

		int col = 0;
		int row = Character.digit(rank, 10) - 1;

		switch (file) {
		case 'a':
			break;
		case 'b':
			col = 1;
			break;
		case 'c':
			col = 2;
			break;
		case 'd':
			col = 3;
			break;
		case 'e':
			col = 4;
			break;
		case 'f':
			col = 5;
			break;
		case 'g':
			col = 6;
			break;
		case 'h':
			col = 7;
			break;
		}

		int[] coordinates = { col, row };
		return coordinates;
	}
	
	
	/**
	 * Decides if this piece has any valid moves from its starting position
	 * @param oldPos Starting position
	 * @param board Chessboard where the move would take place
	 * @return True if move is valid, false otherwise
	 */
	public boolean validMoveExists(String oldPos, Chessboard board) {
		int[] coordinates = new int[2];
		//Piece movingPiece = board.getTile(oldPos).getPiece();
		//Piece targetPiece = null;
		Boolean validMove = false;
		String newPos = "";
		
		Piece[][] pieces = new Piece[8][8];
		
		//Store positions of every piece
		for(int i = 0; i < 8; i++) {
			for(int j = 0; j < 8; j++) {
				coordinates[0] = i;
				coordinates[1] = j;
				newPos = board.coordinatesToFileRank(coordinates);
				pieces[i][j] = board.getTile(newPos).getPiece();
			}
		}
		
		//check every tile and see if it is valid for the moving piece
		for(int i = 0; i < 8  && !validMove; i++) {
			for(int j = 0; j < 8; j++) {
				coordinates[0] = i;
				coordinates[1] = j;
				
				newPos = board.coordinatesToFileRank(coordinates);
				//targetPiece = board.getTile(newPos).getPiece();
				//Check if "oldPos newPos" is a valid move and reset pieces
				validMove = Chess.validInput(board, this.color, oldPos + " " + newPos, false);
				
				//Put all pieces back in original places
				for(int c = 0; c < 8; c++) {
					for(int d = 0; d < 8; d++) {
						coordinates[0] = c;
						coordinates[1] = d;
						newPos = board.coordinatesToFileRank(coordinates);
						board.getTile(newPos).setPiece(pieces[c][d]);
					}
				}
				
				if(validMove) break;
			}
			
			
		}
		
		
		return validMove;
	}

	
	/**
	 * Checks if there are any pieces in the path of the moving piece from
	 * its old position to its new position
	 * @param oldFileRank Starting position of piece
	 * @param newFileRank Ending position of movement
	 * @param direction Direction of movement
	 * @param board Chessboard where move is taking place
	 * @return True if path is clear, false if there are pieces in the way
	 */
	public boolean checkPiecesInBetween(int[] oldFileRank, int[] newFileRank, String direction, Chessboard board) {

		int start, end, x;

		// up and down
		if (direction.equals("ud")) {
			if (newFileRank[1] > oldFileRank[1]) {
				start = oldFileRank[1] + 1;
				end = newFileRank[1] - 1;
			} else {
				start = newFileRank[1] + 1;
				end = oldFileRank[1] - 1;
			}

			for (int i = start; i <= end; i++) {
				int[] currTile = { oldFileRank[0], i };
				//System.out.println(board.getTile(board.coordinatesToFileRank(currTile)).getPiece() + "|"
				//		+ board.coordinatesToFileRank(currTile));
				if (board.getTile(board.coordinatesToFileRank(currTile)).getPiece() != null) {
					return false;
				}
			}
		}

		if (direction.equals("lr")) {
			if (newFileRank[0] > oldFileRank[0]) {
				start = oldFileRank[0] + 1;
				end = newFileRank[0] - 1;
			} else {
				start = newFileRank[0] + 1;
				end = oldFileRank[0] - 1;
			}
			for (int i = start; i <= end; i++) {
				int[] currTile = { i, oldFileRank[1] };
				//System.out.println(board.getTile(board.coordinatesToFileRank(currTile)).getPiece() + "|"
				//		+ board.coordinatesToFileRank(currTile));
				if (board.getTile(board.coordinatesToFileRank(currTile)).getPiece() != null) {
					return false;
				}
			}
		}
		// diagonal
		if (direction.equals("dia")) {		
			x = (Math.abs(oldFileRank[0] - newFileRank[0])-1);
			
			//going up and into the left
			if(newFileRank[1] > oldFileRank[1] && oldFileRank[0] > newFileRank[0]) {
				//System.out.println("1");
				for(int i=0; i<x;i++) {
					int[] currTile = {oldFileRank[0]-1-i, oldFileRank[1]+1+i};
					//System.out.println(coordinatesToFileRank(currTile));
					if(board.getTile(board.coordinatesToFileRank(currTile)).getPiece() != null) {
						//System.out.println(board.getTile(board.coordinatesToFileRank(currTile)).getPiece() + "|"
						//		+ board.coordinatesToFileRank(currTile));
						return false;
					}
				}
			}
			//going up into the right
			if(newFileRank[1] > oldFileRank[1] && newFileRank[0] > oldFileRank[0]) {
				//System.out.println("2");
				for(int i=0; i<x;i++) {
					int[] currTile = {oldFileRank[0]+1+i, oldFileRank[1]+1+i};
					//System.out.println(coordinatesToFileRank(currTile));
					if(board.getTile(board.coordinatesToFileRank(currTile)).getPiece() != null) {
						//System.out.println(board.getTile(board.coordinatesToFileRank(currTile)).getPiece() + "|"
						//		+ board.coordinatesToFileRank(currTile));
						return false;
					}	
				}
			}
			//going down into the left
			if(oldFileRank[1] > newFileRank[1] && oldFileRank[0] > newFileRank[0]) {
				//System.out.println("3");
				for(int i=0; i<x;i++) {
					int[] currTile = {oldFileRank[0]-1-i, oldFileRank[1]-1-i};
					//System.out.println(coordinatesToFileRank(currTile));
					if(board.getTile(board.coordinatesToFileRank(currTile)).getPiece() != null) {
						//System.out.println(board.getTile(board.coordinatesToFileRank(currTile)).getPiece() + "|" 
						//+ board.coordinatesToFileRank(currTile));
						return false;
					}	
				}
			}
			//going down into the right
			if(oldFileRank[1] > newFileRank[1] && newFileRank[0] > oldFileRank[0]) {
				//System.out.println("4");
				for(int i=0; i<x;i++) {
					int[] currTile = {oldFileRank[0]+1+i, oldFileRank[1]-1-i};
					//System.out.println(coordinatesToFileRank(currTile));
					if(board.getTile(board.coordinatesToFileRank(currTile)).getPiece() != null) {
						//System.out.println(board.getTile(board.coordinatesToFileRank(currTile)).getPiece() + "|"
						//		+ board.coordinatesToFileRank(currTile));
						return false;
					}
				}
			}
		}
			
		//System.out.println("Move is unblocked\n");	
		return true;
	}
	
	/**
	 * Checks if the Piece on the targetTile is the same color as this piece
	 * @param targetTile Tile whose Piece will be checked
	 * @return True if pieces are the same color, false otherwise
	 */
	public boolean sameColorCheck(Tile targetTile) {
		// return true for same color and return false for different color
		if (targetTile.getPiece() == null)
			return false;
		if (color.equals(targetTile.getPiece().getColor())) {
			return true;
		}
		return false;
	}
}
