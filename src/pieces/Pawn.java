/**
 * @author Jerry Zhu and Chris Hartley
 */
package pieces;

import chess.Chessboard;

public class Pawn extends Piece{
	
	/*
	 * Whether this pawn can be passed through enpassent
	 */
	public boolean enpassent = false;
	
	/**
	 * Creates a pawn of the chosen color
	 * @param color Player that the pawn belongs to
	 */
	public Pawn(String color){
		super(color);
	}
	
	
	/**
	 * Decides if the proposed move is valid for this pawn
	 * @param oldPos Starting position
	 * @param newPos Proposed new position
	 * @param board Chessboard where the move would take place
	 * @return True if move is valid, false otherwise
	 */
	public boolean validMove(String oldPos, String newPos, Chessboard board) {
		
		//System.out.println("Checking Pawn");
		//System.out.println("Has Moved:"+moved);
		
		int[] oldFileRank =  fileRankToCoordinates(oldPos);
		int[] newFileRank = fileRankToCoordinates(newPos);
		//pawn can only move forward when not capturing
		if(oldFileRank[0] == newFileRank[0]) {
			
			//check for moving only one tile
			if(color.equals("white") && newFileRank[1] == (oldFileRank[1]+1) && board.getTile(newPos).getPiece() == null ) {
				//System.out.println("Moving white pawn up one unit");
				return true;
			}
			if(color.equals("black") && newFileRank[1] == (oldFileRank[1] - 1) && board.getTile(newPos).getPiece() == null) {
				//System.out.println("Moving black pawn down one unit");
				return true;
			}
			
			//check for moving two tiles
			if(color.equals("white") && newFileRank[1] == (oldFileRank[1]+2)) {

				if(board.getTile(newPos).getPiece() != null) return false;
				if(!checkPiecesInBetween(oldFileRank, newFileRank,"ud",board))
					return false;
				//piece can be passed
				enpassent = true;
				return true;
			}
			if(color.equals("black") && newFileRank[1] == (oldFileRank[1]-2)) {

				if(board.getTile(newPos).getPiece() != null) return false;
				
				//System.out.println("moving 2 down");
				if(!checkPiecesInBetween(oldFileRank, newFileRank,"ud",board))
					return false;
				//piece can be passed
				enpassent = true;
				return true;
			}
			
			
			
		}
		
		if(validCapture(oldPos, newPos, board)) return true;
		
		return false;
	}
	
	
	/**
	 * Decides if the proposed move is a valid capture for this pawn
	 * @param oldPos Starting position
	 * @param newPos Proposed new position
	 * @param board Chessboard where the capture would take place
	 * @return True if move is valid, false otherwise
	 */
	public boolean validCapture(String oldPos, String newPos, Chessboard board) {
		int[] oldFileRank =  fileRankToCoordinates(oldPos);
		int[] newFileRank = fileRankToCoordinates(newPos);
		
		if(Math.abs(oldFileRank[0] - newFileRank[0]) == 1 
				&& board.getTile(newPos).getPiece() != null 
				&& !sameColorCheck(board.getTile(newPos))) {
			
			//white capture
			if(color.equals("white") && oldFileRank[1] + 1 == newFileRank[1]) {
				//System.out.println("Capturing:"+board.getTile(newPos).getPiece());
				return true;
			}
			
			//black capture
			if(color.equals("black") && oldFileRank[1] - 1 == newFileRank[1]) {
				//System.out.println("Capturing:" + board.getTile(newPos).getPiece());
				return true;
			}
			
		}
		
		//enpassent capture
		
		//white capture black using en passant
		//pieces should be only one column apart and the new postion should be higher than the old position
		if(color.equals("white") && Math.abs(oldFileRank[0] - newFileRank[0]) == 1 && newFileRank[1] - oldFileRank[1] == 1) {
			int[] tempFileRank = newFileRank;
			tempFileRank[1] = tempFileRank[1] - 1;
			if(board.getTile(board.coordinatesToFileRank(tempFileRank)).getPiece() instanceof Pawn) {
				//System.out.println("enpassent capture time");
				Pawn temp = (Pawn) board.getTile(board.coordinatesToFileRank(tempFileRank)).getPiece();			
				//System.out.println(temp.enpassent);
				if(temp.enpassent == true) {
					return true;
				}				
			}
			
		}
		if(color.equals("black") && Math.abs(oldFileRank[0] - newFileRank[0]) == 1 && oldFileRank[1] - newFileRank[1] == 1) {
			int[] tempFileRank = newFileRank;
			tempFileRank[1] = tempFileRank[1] + 1;
			if(board.getTile(board.coordinatesToFileRank(tempFileRank)).getPiece() instanceof Pawn) {
				//System.out.println("enpassent capture time");
				Pawn temp = (Pawn) board.getTile(board.coordinatesToFileRank(tempFileRank)).getPiece();			
				//System.out.println(temp.enpassent);
				if(temp.enpassent == true) {
					return true;
				}
			}
				
		}
		
		
		return false;
	}
	
	
	public String toString() {
		return this.color.charAt(0)+"p";
	}
}
