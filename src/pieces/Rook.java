/**
 * @author Jerry Zhu and Chris Hartley
 */
package pieces;

import chess.Chessboard;

public class Rook extends Piece{
	
	
	/**
	 * Creates a rook of the chosen color
	 * @param color Player that the rook belongs to
	 */
	public Rook(String color){
		super(color);
	}
	
	/**
	 * Decides if the proposed move is valid for this rook
	 * @param oldPos Starting position
	 * @param newPos Proposed new position
	 * @param board Chessboard where the move would take place
	 * @return True if move is valid, false otherwise
	 */
	public boolean validMove(String oldPos, String newPos, Chessboard board) {
		
		//System.out.println("Checking rook");
		
		//check if newPos is in the same row or column as the oldPos
		int[] oldFileRank = fileRankToCoordinates(oldPos);
		int[] newFileRank = fileRankToCoordinates(newPos);
		
		//moving up and down
		if (oldFileRank[0] == newFileRank[0] && !sameColorCheck(board.getTile(newPos))
				&& checkPiecesInBetween(oldFileRank, newFileRank, "ud", board)) {
			return true;
		}
		//moving left to right
		if (oldFileRank[1] == newFileRank[1] && !sameColorCheck(board.getTile(newPos))
				&& checkPiecesInBetween(oldFileRank, newFileRank, "lr", board)) {
			return true;
		}
			
		return false;
	}
	
	public String toString() {
		return this.color.charAt(0)+"R";
	}
}
