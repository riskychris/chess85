/**
 * @author Jerry Zhu and Chris Hartley
 */
package chess;
import pieces.*;

public class Chessboard {
	/*
	 * 8x8 array containing the Chessboard's game tils
	 */
	public Tile[][] tiles = new Tile[8][8];
	
	/*
	 * Coordinates of pawn that can be passed through enpassent
	 */
	public int[] epCoord = {-1,-1};
	
	/*
	 * Coordinates of the previous pawn that could be passed through enpassent
	 */
	public int[] oldEpCoord = {-1, -1};
	
	
	/**
	 * Creates an 8x8 board of tiles and places the white and black
	 * pieces in their initial positions
	 */
	public Chessboard() {
		//Initialize board of empty Tiles
		for(int r = 0; r < 8; r++) {
			for(int c = 0; c < 8; c++) {
				//black tiles
				if((c+r)%2 == 0) tiles[c][r] = new Tile("black");
				
				//white tiles
				else tiles[c][r] = new Tile("white");
			}
		}
		
		//Place pieces on board
		this.resetPieces();
	}
	
	
	/**
	 * Places the white and black pieces in their initial positions
	 */
	public void resetPieces() {
		//reset black
		tiles[0][7].setPiece(new Rook("black"));
		tiles[7][7].setPiece(new Rook("black"));
		tiles[1][7].setPiece(new Knight("black"));
		tiles[6][7].setPiece(new Knight("black"));
		tiles[2][7].setPiece(new Bishop("black"));
		tiles[5][7].setPiece(new Bishop("black"));
		tiles[3][7].setPiece(new Queen("black"));
		tiles[4][7].setPiece(new King("black"));
		for(int i = 0; i < 8; i++) {
			tiles[i][6].setPiece(new Pawn("black"));
		}
				

		//reset white
		tiles[0][0].setPiece(new Rook("white"));
		tiles[7][0].setPiece(new Rook("white"));
		tiles[1][0].setPiece(new Knight("white"));
		tiles[6][0].setPiece(new Knight("white"));
		tiles[2][0].setPiece(new Bishop("white"));
		tiles[5][0].setPiece(new Bishop("white"));
		tiles[3][0].setPiece(new Queen("white"));
		tiles[4][0].setPiece(new King("white"));
		
		for(int i = 0; i < 8; i++) {
			tiles[i][1].setPiece(new Pawn("white"));
		}
		
		/*pieces to test promotion
		tiles[1][1].setPiece(new Pawn("black"));
		tiles[1][6].setPiece(new Pawn("white"));
		*/
		
		/*pieces to test checkmate
		tiles[3][6].setPiece(null);
		tiles[4][6].setPiece(null);
		tiles[1][0].setPiece(null);
		tiles[6][0].setPiece(null);
		tiles[4][4].setPiece(new Queen("white"));
		tiles[2][3].setPiece(new Queen("white"));
		*/
	}
	
	/**
	 * Converts integer coordinates to FileRank coordinates
	 * @param coordinates A 2-element integer array corresponding a Chessboard tile
	 * @return The coordinates expressed in FileRank format
	 */
	public  String coordinatesToFileRank(int[] coordinates) {
		String fileRank = "";
		String col = "";

		switch (coordinates[0]) {
		case 0:
			col = "a";
			break;
		case 1:
			col = "b";
			break;
		case 2:
			col = "c";
			break;
		case 3:
			col = "d";
			break;
		case 4:
			col = "e";
			break;
		case 5:
			col = "f";
			break;
		case 6:
			col = "g";
			break;
		case 7:
			col = "h";
			break;
		}

		fileRank = col + (coordinates[1] + 1);

		return fileRank;
	}
	
	/**
	 * Converts FileRank coordinates to integer coordinates
	 * @param fileRank The coordinates expressed in FileRank format
	 * @return A 2-element integer array corresponding to a Chessboard tile
	 */
	public int[] fileRankToCoordinates(String fileRank){
		char file = fileRank.charAt(0);
		char rank = fileRank.charAt(1);
		
		int col = 0;
		int row = Character.digit(rank, 10) - 1;

		
		switch(file) {
		case 'a': break;
		case 'b': col = 1; break;
		case 'c': col = 2; break;
		case 'd': col = 3; break;
		case 'e': col = 4; break;
		case 'f': col = 5; break; 
		case 'g': col = 6; break;
		case 'h': col = 7; break;
		}
		
		int[] coordinates = {col, row};
		return coordinates;
	}
	
	
	/**
	 * Checks if the given player's king is in check
	 * @param player Player whose king is being investigates
	 * @param kingPos Position of the player's king
	 * @param enemyPieces[] Array of enemy pieces
	 * @param enemyPos[] Positions of enemy ieces
	 * @param enemyCount Number of enemy pieces
	 * @return True if the player's king is in check, false otherwise
	 */
	public Boolean kingInCheck(String player, String kingPos, Piece[] enemyPieces, String enemyPos[], int enemyCount ) {
		Piece piece;
		for(int i = 0; i < enemyCount; i ++) {
			piece = enemyPieces[i];
			if(piece.validMove(enemyPos[i], kingPos, this)) {
				//System.out.println(player + " is in check");
				return true;
			}
		}
		//System.out.println();
		return false;
	}
	
	
	/**
	 * Checks if the given player's king is in check
	 * @param player Player whose king is being investigates
	 * @return True if the player's king is in check, false otherwise
	 */
	public Boolean kingInCheck(String player) {
		String kingPos = "";
		Piece piece;
		int[] coordinates = new int[2];
		Piece[] enemyPieces = new Piece[16];
		String[] enemyPos = new String[16];
		int enemyCount = 0;
		
		//Find king and enemy pieces
		for(int r = 7; r >= 0; r--) {	
			//check each cell in column
			for(int c = 0; c < 8; c++) {
				piece = this.tiles[c][r].getPiece();
				
				if(piece == null) continue;
				
				coordinates[0] = c;
				coordinates[1] = r;
				
				//if piece is the player's king
				if(piece.getColor().equalsIgnoreCase(player.toLowerCase())) {
					if (piece.getClass().equals(King.class)) kingPos = coordinatesToFileRank(coordinates);
				}
					
				else {
					//if the piece is not null and not the players
					enemyPieces[enemyCount] = piece;
					enemyPos[enemyCount] =  coordinatesToFileRank(coordinates);
					enemyCount++;
				}
				
			}
			
		}
		return kingInCheck(player, kingPos, enemyPieces, enemyPos, enemyCount);
		
	}
	
	
	/**
	 * Checks if the given player's king is in checkmate
	 * @param player Player whose king is being investigates
	 * @return True if the player's king is in checkmate, false otherwise
	 */
	public Boolean kingInCheckmate(String player) {
		Boolean validMove = false;
		String pos = "";
		String kingPos = "";
		Piece piece;
		int[] coordinates = new int[2];
		Piece[] enemyPieces = new Piece[16];
		String[] enemyPos = new String[16];
		int enemyCount = 0;
		Piece[] friendlyPieces = new Piece[16];
		String[] friendlyPos = new String[16];
		int friendlyCount = 0;
		
		
		for(int r = 7; r >= 0; r--) {	
			//check each cell in column
			for(int c = 0; c < 8; c++) {
				piece = this.tiles[c][r].getPiece();
				
				if(piece == null) continue;
				
				coordinates[0] = c;
				coordinates[1] = r;
				
				//if piece is the player's king
				if(piece.getColor().equalsIgnoreCase(player.toLowerCase())) {
					if (piece.getClass().equals(King.class)) {
						kingPos = coordinatesToFileRank(coordinates);
						//kingCoordinates = coordinates;
					}
			
					friendlyPieces[friendlyCount] = piece;
					friendlyPos[friendlyCount] =  coordinatesToFileRank(coordinates);
					friendlyCount++;
					
				}
					
				else {
					//if the piece is not null and not the players
					enemyPieces[enemyCount] = piece;
					enemyPos[enemyCount] =  coordinatesToFileRank(coordinates);
					enemyCount++;
				}
				
			}
			
		}
		
		if (kingInCheck(player, kingPos, enemyPieces, enemyPos, enemyCount)) System.out.print("\nCheck"); 
		else return false;
		
		
		for(int i = 0; i < friendlyCount && !validMove; i++) {
			piece = friendlyPieces[i];
			pos = friendlyPos[i];
			//System.out.println("Checking: " + piece);
			if(piece.validMoveExists(pos, this)) validMove = true;
			
		}
		
		if(!validMove)System.out.println("mate");
		
		return !validMove;
	}
	
	/**
	 * Moves piece from oldPos to newPos on the given board
	 * @param board Chessboard move is being performed on
	 * @param oldPos Position of the piece being moved
	 * @param newPos Position the piece is being moved to
	 */
	public void makeMove(Chessboard board, String oldPos, String newPos) {
		
		boolean epmove = false;;
		int[] oldFileRank = fileRankToCoordinates(oldPos);
		int[] newFileRank = fileRankToCoordinates(newPos);
		Piece movePiece = board.getTile(oldPos).getPiece();
		
		if(board.getTile(newPos).getPiece() != null) {
			//System.out.println(board.getTile(oldPos).getPiece()+" Capturing "+board.getTile(newPos).getPiece());
		}
		
		//castle movement
		if(movePiece instanceof King && Math.abs(oldFileRank[0] - newFileRank[0]) == 2) {
			castleMove(oldFileRank,newFileRank);
		}
		
		//movement for enpassent
		//getting clearing the square below the new position
		if(movePiece instanceof Pawn) {
			
			
			//enpassent property only lasts for one turn
			if(Math.abs(newFileRank[1] - oldFileRank[1]) == 2) {
				//if no piece has had the property yet
				if(epCoord[0] == -1) {
					epCoord[0] = newFileRank[0];
					epCoord[1] = newFileRank[1];
				}
				
				else {
					//If piece is a pawn, allow opening move
					if(board.getTile(coordinatesToFileRank(epCoord)).getPiece() instanceof Pawn) {
						Pawn set = (Pawn) board.getTile(coordinatesToFileRank(epCoord)).getPiece();
						set.enpassent = false;
						oldEpCoord[0] = epCoord[0];
						oldEpCoord[1] = epCoord[1];
						epCoord[0] = newFileRank[0];
						epCoord[1] = newFileRank[1];
					}
					
					//Otherwise the pawn's attempting opening move had invalid extra information
					//So a new move was requested and the previous pawn can be taken enpassent
					else {
						epCoord[0] = oldEpCoord[0];
						epCoord[1] = oldEpCoord[1];
					}
				}
			}
			if(Math.abs(newFileRank[1] - oldFileRank[1]) == 1) {
				int[] tempFileRank = {newFileRank[0],oldFileRank[1]};
				String temp = coordinatesToFileRank(tempFileRank);
				if(board.getTile(temp).getPiece() instanceof Pawn) {
					Pawn tempP = (Pawn) board.getTile(temp).getPiece();
					if(tempP.enpassent == true) {
						tiles[tempFileRank[0]][tempFileRank[1]] = new Tile(board.getTile(temp).getColor());
						epmove = true;
						epCoord[0] = -1;
						epCoord[1] = -1;
					}
				}
				if(epCoord[0] != -1 && epmove == false && board.getTile(newPos).getPiece() != null) {
					System.out.println(epCoord[0]+","+epCoord[1]);
					if(board.getTile(coordinatesToFileRank(epCoord)).getPiece() instanceof Pawn) {
						Pawn set = (Pawn) board.getTile(coordinatesToFileRank(epCoord)).getPiece();
						set.enpassent = false;
						epCoord[0] = -1;
					}
				}
				
			}
			
						
		}
		else {
			//setting enpassent back to false
			if(epCoord[0] != -1) {
				if(board.getTile(coordinatesToFileRank(epCoord)).getPiece() instanceof Pawn) {
				Pawn set = (Pawn) board.getTile(coordinatesToFileRank(epCoord)).getPiece();
				set.enpassent = false;
				epCoord[0] = -1;
				epCoord[1] = -1;
				}
			}
		}
		//end of enpassent
		
		
		tiles[oldFileRank[0]][oldFileRank[1]] = new Tile(board.getTile(oldPos).getColor());
		tiles[newFileRank[0]][newFileRank[1]].setPiece(movePiece);
		
		//the piece has moved
		movePiece.moved = true;
	}
	
	
	/**
	 * Performs castle move with king going from from oldFileRank to newFileRank
	 * @param oldFileRank[] Integer coordinates of the piece being moved
	 * @param newFileRank[] Integer coordinates that the piece is being moved to
	 */
	public void castleMove(int[] oldFileRank,int[] newFileRank) {
		//move the rook	
		if(newFileRank[0] == 6) {
			if(newFileRank[1] == 0) {
				tiles[5][0].setPiece(new Rook("white"));
				tiles[7][0] = new Tile("white");
			}
			//if newFileRank[1] == 7
			else {
				tiles[5][7].setPiece(new Rook("black"));
				tiles[7][7] = new Tile("black");
			}
		}
		//if newFileRank[0] == 2
		else {
			if(newFileRank[1] == 0) {
				tiles[3][0].setPiece(new Rook("white"));
				tiles[0][0] = new Tile("black");
			}
			//if newFileRank[1] == 7
			else {
				tiles[3][7].setPiece(new Rook("black"));
				tiles[0][7] = new Tile("white");
			}
		}
	}
	
	
	/**
	 * Get tile from fileRank coordinates
	 * @param fileRank FileRank coordinates of the tile
	 * @return The tile at given fileRank position
	 */
	public Tile getTile(String fileRank) {
		int[] coordinates = fileRankToCoordinates(fileRank);
		
		return this.tiles[coordinates[0]][coordinates[1]];
	}
	
	/**
	 * Prints this board as required by project specifications
	 */
	public void print() {
		//(col, row) = (0,0) = (a,1) is the bottom left cell
		
		//print rows from top (row 7) to bottom
		for(int r = 7; r >= 0; r--) {
			
			//print each cell in column
			for(int c = 0; c < 8; c++)
				System.out.print(tiles[c][r] + " ");
			
			System.out.println(r+1);
		}
		System.out.println(" a  b  c  d  e  f  g  h");
		
	}
	
	
}
