/**
 * @author Jerry Zhu and Chris Hartley
 */
package chess;

import pieces.Piece;

public class Tile {
	
	/*
	 * Color of this tile
	 */
	public String color;
	
	/*
	 * The piece the occupies this tile (may be null)
	 */
	public Piece piece;
	
	/**
	 * Creates a new tile of the given color
	 * @param color Color of the tile
	 */
	public Tile(String color) {
		this.color = color.toLowerCase();
	}
	
	/**
	 * Retrieve this tile's piece
	 * @return The Piece on this Tile
	 */
	public Piece getPiece() {
		return this.piece;
	}
	
	/**
	 * Set this tile's piece
	 * @param piece Piece that will be placed on this Tile
	 */
	public void setPiece(Piece piece) {
		this.piece = piece;
	}
	
	/**
	 * Retrieve color of this tile
	 * @return Color of this tile
	 */
	public String getColor() {
		return this.color;
	}
	
	public String toString() {
		if(piece == null ){
			if (color.equals("white")) return "  ";
			else return "##";
		}
		return piece.toString();
	}
}
