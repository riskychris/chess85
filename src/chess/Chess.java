/**
 * @author Jerry Zhu and Chris Hartley
 */
package chess;

import java.util.Scanner;

import pieces.*;

public class Chess {

	private static Scanner userInput = new Scanner(System.in);

	public static void main(String[] args) {
		Chessboard board = new Chessboard();
		playGame(board);
	}
	
	
	/**
	 * Plays a game of Chess with a Chessboard instance
	 * @param board Chessboard object that holds all the pieces
	 */
	public static void playGame(Chessboard board) {
		Boolean draw = false;
		Boolean noWinner = true;
		String[] players = {"White", "Black"};
		String player = "";
		String move = "";
		int turnNum = 0;

		board.print();
		// Play the game while nobody has won
		for (; noWinner; turnNum++) {
			player = players[turnNum % 2];

			move = requestMove(board, player, draw);

			// resign check
			if (move.equalsIgnoreCase("resign")) {
				System.out.println(players[(turnNum+1) % 2] + " wins");
				return;
			}
			
			//draw check
			if(move.equalsIgnoreCase("draw") && draw == true) {
				//System.out.println("Draw!");
				break;
			}
			
			String[] positions = move.split(" ");
			//draw pending setup
			if(positions.length == 3) {
				String extra = positions[2];
				if(extra.equals("draw?")){
					draw = true;
					continue;
				}
			}

			draw = false;
			
			board.print();
			//Did this move put the other player in check
			if(board.kingInCheckmate(players[(turnNum+1) % 2])) {
				noWinner = false;
			}

		}
		//System.out.println();
		if(draw == true) System.out.println("Draw!");
		else System.out.println(player + " wins!");
	
	}
	/**
	 * Request and validates user input on the given Chessboard
	 * @param board Chessboard that moves will be validated on
	 * @param player The moving player
	 * @param drawCalled True if other player requested a draw, false otherwise
	 * @return A correct input string
	 */
	public static String requestMove(Chessboard board, String player, Boolean drawCalled) {
		String input = "";
		//System.out.println();
		// break condition handled in loop
		System.out.println();
		while (true) {
			System.out.print(player + "'s move: ");
			input = userInput.nextLine();

			if (validInput(board, player, input, drawCalled))
				break;
			else
				System.out.println();
				System.out.println("Illegal move, try again.\n");
		}
		System.out.println();
		return input;
	}
	/**
	 * Validates user input on the given Chessboard, and makes move
	 * @param board Chessboard that moves will be validated on
	 * @param player The moving player
	 * @param input The proposed move
	 * @param drawCalled True if other player requested a draw, false otherwise
	 * @return True if input is valid
	 */
	public static boolean validInput(Chessboard board, String player, String input, Boolean drawCalled) {
		String oldPos;
		String newPos;
		String extra = null;
		String[] moves = input.split(" ");
		
		// if the input is resign
		if (input.equalsIgnoreCase("resign"))
			return true;

		if (input.equalsIgnoreCase("draw"))
			return drawCalled;

		// If less or more than 2 moves
		//pending special input check
		if (moves.length == 3) {
			extra = moves[2];
			switch(extra) {
			case "draw?": break;
			case "Q": break;
			case "B": break;
			case "N": break;
			case "R": break;
			default: return false;
			}
		}

		else if (moves.length != 2) return false;

		// If either move is not 2 char
		oldPos = moves[0];
		newPos = moves[1];
		if (oldPos.length() != 2 || newPos.length() != 2)
			return false;

		// If the moves are not valid board tiles
		if (!validBoardTile(oldPos) || !validBoardTile(newPos))
			return false;

		// If the player does not have a piece on the starting tile
		Piece piece = board.getTile(oldPos).getPiece();
		if (!ownsPieceOnTile(player, piece))
			return false;

		
		// If the newPos is an invalid move for the piece
		if (!piece.validMove(oldPos, newPos, board))
			return false;

		// If the moves are the same
		if (oldPos.equalsIgnoreCase(newPos))
			return false;
		
		//Temporarily store the target piece
		Piece movingPiece = board.getTile(oldPos).getPiece();
		Piece targetPiece = board.getTile(newPos).getPiece();
		
		//Move Piece
		board.makeMove(board, oldPos, newPos);
		
		//If move puts player in check, put movingPiece back on oldPos and restore the target piece to newPos
		if(board.kingInCheck(player.toLowerCase())) {
			//System.out.println("This move would put me in check");
			board.getTile(oldPos).setPiece(movingPiece);
			board.getTile(newPos).setPiece(targetPiece);
			return false;
		}
		
		//Move is valid, so promote pawn if necessary
		if(piece instanceof Pawn) {
			int[] coordinates = board.fileRankToCoordinates(newPos);
			
			//If black pawn is moving to rank 1 or white pawn is moving to rank 8
			if( (piece.getColor().equals("black") && coordinates[1] == 0) || (piece.getColor().equals("white")&& coordinates[1] == 7) ) {
				
				if (extra == null){
					board.getTile(newPos).setPiece(new Queen(player));
				}
				else {
					switch(extra) {
					case "B": board.getTile(newPos).setPiece(new Bishop(player)); break;
					case "N": board.getTile(newPos).setPiece(new Knight(player)); break;
					case "R": board.getTile(newPos).setPiece(new Rook(player)); break;
					case "Q": board.getTile(newPos).setPiece(new Queen(player)); break;
					case "draw?": board.getTile(newPos).setPiece(new Queen(player)); break;
					}
				}
				
			}
				
			// If the pawn cannot be promoted and the extra info is not "draw?", undo move and flag input as invalid
			else if(extra != null && !(extra.equals("draw?"))){
				board.getTile(oldPos).setPiece(movingPiece);
				board.getTile(newPos).setPiece(targetPiece);
				return false;
			}
			
		}
		
		//if piece is not a pawn and the extra info is not "draw?"
		else if(extra != null && !(extra.equals("draw?"))){
			board.getTile(oldPos).setPiece(movingPiece);
			board.getTile(newPos).setPiece(targetPiece);
			return false;
		}
		
		//System.out.println("Correct Input!");
		return true;

	}
	/**
	 * Checks to see if the input is a correct tile on the board
	 * @param move Proposed tile
	 * @return boolean True input is referring to a tile on the board, false otherwise
	 */
	public static boolean validBoardTile(String move) {
		char col = move.charAt(0);
		char row = move.charAt(1);

		// Is move is in the right format
		Boolean valid = Character.isLetter(col) && Character.isDigit(row);
		if (!valid)
			return false;

		// Is column input between [a, h]
		valid = Character.compare(col, 'a') >= 0 && Character.compare(col, 'h') <= 0;
		if (!valid)
			return false;

		// Is row input between [1, 8]
		valid = Character.compare(row, '1') >= 0 && Character.compare(row, '8') <= 0;
		return valid;
	}
	/**
	 * Checks to see if piece taken from a board tile belongs to player
	 * @param player Proposed owner of piece
	 * @param piece Piece to be checked
	 * @return True if piece belongs to player, false otherwise
	 */
	public static Boolean ownsPieceOnTile(String player, Piece piece) {
		if (piece == null)
			return false;
		return piece.getColor().equalsIgnoreCase(player.toLowerCase());
	}
	
}
